const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));

//const fs = require('fs');
const cors = require('cors');

// Database MongoDB information
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb+srv://workshop:railmp123@cluster0.l5spr.mongodb.net/workshop?retryWrites=true&w=majority";
//const url = "mongodb://localhost/27017/";
const mydatabase = "workshop";
const mycollection = "customers";

// Resolvendo o CORS em desenvolvimento
app.use((req, res, next) => {
	//Qual site tem permissão de realizar a conexão, no exemplo abaixo está o "*" indicando que qualquer site pode fazer a conexão
    res.header("Access-Control-Allow-Origin", "*");
	//Quais são os métodos que a conexão pode realizar na API
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    app.use(cors());
    next();
});

app.get('/', (req, res) => {
    //console.log('Got body:', req.body);
    
    // Taking the currente date-time
    var today = new Date();
    console.log("Server is running.");
	console.log(today);	
	res.send("Server is running. Date/time is: "+today);
});

app.post('/insereDados', async (req, res) => {
    console.log('Got body:', req.body);
	
	//var entraDados = req.body.entraDados.toString();
	var nome = req.body.nome.toString();
	var idade = parseInt(req.body.idade);
	
	console.log("nome: "+nome+", idade: "+idade );	
	
	MongoClient.connect(url, function(err, db) {
	if (err) throw err;
		var dbo = db.db(mydatabase);
		var myobj = {nome: nome, idade: idade };

	dbo.collection(mycollection).insertOne(myobj, function(err, result) {
    if (err) throw err;
    console.log("1 document inserted");
    res.send("1 document inserted.");
    db.close();
	  });
	});
	
});

app.get('/lerDados', async  (req, res) => {
    console.log('Got body:', req.body);
	
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var dbo = db.db(mydatabase);
	  dbo.collection(mycollection).find({}).toArray(function(err, result) {
	    if (err) throw err;

		/*var felinos = [];
		result.forEach(function (item, indice, array) {
			if (indice == 0) {
				felinos = item;
			}
		    console.log(array[indice], indice);
		});
	
		console.log(felinos);
		res.send(felinos);*/
		
		console.log(result);
		console.log("result: "+typeof result);
		console.log("is array? "+Array.isArray(result));
		console.log(Date.UTC());
		res.json(result);   // Enviando resposta para o frontend como objeto JSON
	    db.close();
	  });
	});  
    	    
});

app.post('/atualizaDados', async (req, res) => {
    console.log('Got body:', req.body);
	
	//var entraDados = req.body.entraDados.toString();
	var nome = req.body.nome.toString();
	var idade = parseInt(req.body.idade);
	console.log('nome: '+nome+', idade: '+idade*10);
	
	
	// Fazer update dos dados do formulário
	MongoClient.connect(url, function(err, db) {
		  if (err) throw err;
		  var dbo = db.db(mydatabase);
		  var myquery = { nome: nome };
		  var newvalues = { $set: { nome: nome, idade: idade } };
		  dbo.collection(mycollection).updateOne(myquery, newvalues, function(err, result) {
		    if (err) throw err;
		    if (result.matchedCount != 0)	{
		    	console.log("1 document updated");
		    	res.send("1 document updated.");			    	
		    } else {
		    	console.log("Record not found");
		    	res.send("Record not found.");
		    }
		    
		    db.close();
		  }); // end dbo updatedOne()
		}); // end connect updatedOne()
		
});	// end app.post

app.post('/deletaDados', async (req, res) => {
    console.log('Got body:', req.body);
	
	//var entraDados = req.body.entraDados.toString();
	var nome = req.body.nome.toString();
	
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var dbo = db.db(mydatabase);
	  var myquery = { nome: nome };
	  dbo.collection(mycollection).deleteOne(myquery, function(err, obj) {
	    if (err) throw err;
	    console.log("1 document deleted");
	    //console.log(obj);
	    res.send("1 document deleted.");
	    
	    db.close();
	  });
	});
		
});	

app.listen(3009, () => console.log('Started server at http://localhost:3009!'));